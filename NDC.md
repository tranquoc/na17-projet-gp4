# Note de clarification
## Projet de conception et de réalisation de la base de données d'une place d'échange à UTC

### Contexte du projet
Dans le cadre de l’UV NA17 le présent projet concerne la conception et la réalisation d’une base de donnée de gestion d’une place d'échange à UTC.

## Acteurs du projet
Maîtrise d’ouvrage : M. Z administrateur de la place d'échange UTC, représenté par M. Crozat

Maîtrise d’œuvre :
- TRAN Quoc Gia Cat
- BURDY Adrien
- PERRIN Oubine
- CORTAL Gustave
- SARBOUT Ilias    

## Données d'entrée sur les différentes composantes du projet

- Aspect technique
    - Cahier des charges fourni par la MOA (référencé https://librecours.net/exercice/projet/projet.xhtml?part=z)
    - Définition des contraintes techniques (référencé https://librecours.net/exercice/projet/cadre.xhtml?part=z)

- Aspect projet
    - Définition du cadre du projet (référencé https://librecours.net/exercice/projet/cadre.xhtml?part=z)
    - Définition des livrables attendus (référencé https://librecours.net/exercice/projet/livrables-nx18.xhtml?part=z)


## Propriétés des objets principaux de la base

### Utilisateur >> Administrateur
* Pseudo : chaîne de caractères [clé]
* Mot de passe
* Date de naissance: date
* Solde: nombre réel positif à 2 chiffres après la virgule
* Mail étudiant : au format @etu.utc.fr
* Les administrateurs sont des utilisateurs qui ont accès à plus de  données

### Objet de l'échange >> Bien, service
* Numéro [clé]
* Titre : nom de l'objet ou du service
* Description
* Service
    * Lieu : chaîne de caractères
    * Date
* Bien
    * État (neuf, bon état, médiocre, mauvais)
    * Quantité: entier

## Fonctions
Les utilisateurs peuvent effectuer certaines actions. Les objets de cette rubriques matérialisent ces actions dans la base. Les utilisateurs ayant les droits d'administrateurs peuvent faire des actions en plus que les utilisateurs normaux.


### Annonces >> Offre, demande
Cet objet permet à un utilisateur de poster une annonce sur le site. Une annonce porte sur un objet de la table "objet de l'échange". Elle peut être soit de type offre, l'utilisateur propose un bien ou un service) soit de type demande, l'utilisateur cherche un bien ou un service.
* Validation (bool) ne peut être changé que par des administrateurs.

* Numéro [clé]
*Date de publication
* Titre
* Description
* Négociable : booléen qui statue si l'annonce est négociable ou non
*Ouvert aux enchères : oui ou non
* Offre
    * Prix : nombre réel
* Les annonces concerne un objet d'échange (bien ou service)
* Les annonces sont publiées par un utilisateur
* Les annonces possèdent obligatoirement un titre et une description

### Transactions
Cet objet est la réalisation d'une annonce. Lorsqu'un utilisateur répond à l'annonce d'un autre utilisateur, alors la transaction est lancée. Une fois que le paiement et/ou l'accord entre les deux partis a été réalisé, l'objet transaction est créé avec le statut en cours de validation. Une fois que les deux partis confirment que la transaction a été effectuées, celle-ci passe en statut "terminée".

* Numéro de l'annonce [clé]
* Date de paiement
* Date de validation de la transaction
* Statut: en cours de validation / terminée
* Montant : nombre réel à 2 chiffres après la virgule
* Les transactions concerne une annonce
* Les transactions se font entre deux utilisateurs
Contraintes:

    - Un utilisateur ne peut pas réaliser une transaction avec lui même

    - La date de paiement correspond à la création de la transaction et le statut est alors "en cours de validation". Une fois la transaction effectuée physiquement par les utilisateurs, le statut de



### Enchère
* Numéro de l'enchère
* Montant: nombre réel à 2 chiffres après la virgule
*Date : date précise
* Les enchères portent sur une annonce
* Les enchères sont effectuées par un utilisateur
Contraintes :

    - Les enchères sur une  même annonce doivent avoir des prix croissants


### Actions >>  Rechargement, Retrait
* Date
* Montant : réel


### Message
* Titre
* Contenu
* Date d'envoi : date
* Un message est envoyé par un utilisateur à un autre utilisateur

### Commentaire
* Titre
* Contenu
* Date
* Les commentaires sont écrit par un utilisateur sur un autre utilisateur au sujet d'une transaction


| Document : Note de clarification version 2 | Date: 11/10/2019 |
| -------------------------------------------| ---------------- |

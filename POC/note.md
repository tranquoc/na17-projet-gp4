# Diagramme de répertoire

|-PPOC
    |- database.ini ----------------> informations de connexion de postgresql
    |- poc.py ----------------------> main 
    |- pocv2.py --------------------> test de andriend
    |- db
        |- __init__.py 
        |- requetes
            |- __init__.py
            |- commande.py ---------> les fonctions de postgresql includes inserer, selectioner, mise a jour
            |- annonce.py ----------> les fonctions de requete de l'annonce includes inserer, selectioner, mise a jour
            |- utilisateur.py ------> les fonctions de requete de l'utilisateur includes inserer, selectioner, mise a jour
        |- sql ---------------------> les fichiers .sql
        |- annonce.py --------------> les main fonctions de l'annonce includes inserer, selectioner, mise a jour
        |- config.py ---------------> le fonction config des informations de connexion de postgresql
        |- entrez.py ---------------> les fonctions de verifier les values que sont entrees par clavier sont vraie ou faux
        |- utilisateur.py ----------> les main fonctions de l'annonce includes inserer, selectioner, mise a jour

# Courir
1. Modifiez les informations de connexion de postgresql dans le fichier database.ini
2. terminal: python3 poc.py
   
# Mission:
1. Creer les fonctions pour inserer Offre, Demande, Bien, Service
2. Creer les fonctions pour modifier Annoce, Offre, Demande, Bien, Service
3. Creer les fonctions pour suprimer Annoce, Offre, Demande, Bien, Service
4. Creer les fonctions pour afficher Annoce, Offre, Demande, Bien, Service
5. Creer la fonction pour modifier l'attribute utilisateur de Annoce quand on modifie le pseudo de Utilisateur

# Modification des informations de connexion postgreSQL dans le fichier db/database.ini
from db.requetes.commande import connect, disconnect, create_table_if_not_exists, suprimer_tableaux
from db.utilisateur import *
from db.annonce import *
from db.entrez import entrez_int

def connect_db():
    conn = None
    try:
        conn = connect()
    except ValueError:
        print ("Pas ouvert database")
        return False
    return conn

def connect_tableaux(conn):
    print("""
====== Connect DB =====
1) Suprimez les tableaux et recreez les nouvelles
2) Reutilisez les anciens tableaux
        """)
    
    choix = entrez_int()
    while choix is False or choix < 1 or choix >2:
            choix = entrez_int()
    if choix == 1:
        suprimer_tableaux(conn)
        create_table_if_not_exists(conn)
    else:
        return False

def deja_connecte(conn):
    pseudo = connecte_utilisateur(conn)
    while pseudo:
        print("""
====== Utilisateur: {} ======
1) Information utilisateur
2) Mise a jour utilisateur
3) Toutes utilisateur
4) Inserer une annonce
5) Disconnecte
            """.format(pseudo))
        
        choix = entrez_int()
        while choix is False or choix < 1 or choix >5:
             choix = entrez_int()
             
        if choix == 1:
            infos = info_utilisateur(conn,pseudo)
            
        elif choix == 2:
            tmp = maj_utilisateur(conn,pseudo)
            if tmp:
                pseudo = tmp
        elif choix == 3:
            info_tout_utilisateur(conn)
        elif choix == 4:
            inserer_annonce(conn, pseudo)
        else:
            return None
            

def main(conn):

    while 1:
        print("""
====== Main ======
1) Inscription utilisateur
2) Se connecter
3) Sortir
        """)

        choix = entrez_int()
        while choix is False or choix < 1 or choix >3:
             choix = entrez_int()
        
        if choix == 1:
            inscrire_utilisateur(conn)
        elif choix == 2:
            deja_connecte(conn)
        elif choix == 3:
            return None
    

if __name__ == '__main__':
    conn = connect_db()
    if conn:
        connect_tableaux(conn)
        main(conn)
    
    print("\n====== Fermer POC ======")
    
    disconnect(conn)
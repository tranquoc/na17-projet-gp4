import psycopg2
from db.config import config
import datetime

def connect():
    conn = None
    try:
        # lire les paramètres de connexion
        params = config()

        # se connecter au serveur PostgreSQL
        conn = psycopg2.connect(**params)
        
        print ("Connect base de donne success ")

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    return conn

def disconnect(conn):
    if conn is not None:
            conn.close()

def suprimer_tableaux(conn, filename="db/sql/drop.sql"):
    try:
        # créer un curseur
        cur = conn.cursor()

        # récupérer toutes les données
        cur.execute(open(filename, "r").read())
        
        # Commit (transactionnal mode is by default)
        conn.commit()
        
        print("Suprmiez des tables")
        return True
        
    except (Exception, psycopg2.Error) as error:
        print(error)
        return False
    finally:
        if conn:
            # fermer la communication avec PostgreSQL
            cur.close()


def create_table_if_not_exists(conn, filename="db/sql/create.sql"):
    try:
        # créer un curseur
        cur = conn.cursor()

        # récupérer toutes les données
        cur.execute(open(filename, "r").read())
        
        # Commit (transactionnal mode is by default)
        conn.commit()
        
        print("Deja il y a des tables")
        return True
        
    except (Exception, psycopg2.Error) as error:
        print(error)
        return False
    finally:
        if conn:
            # fermer la communication avec PostgreSQL
            cur.close()

def selectionner(conn, sql):
    data = None
    try:
        data = []
        # créer un curseur
        cur = conn.cursor()

        # récupérer toutes les données
        cur.execute(sql)

        data = cur.fetchall()
        # print(data)
        # data.append(raw)
        # while raw:
        #     raw = cur.fetchone()
        #     data.append(raw)
    except (Exception, psycopg2.Error) as error:
        print(error)
    finally:
        if conn:
            # fermer la communication avec PostgreSQL
            cur.close()

    return data

def mise_a_jour(conn, sql):
    try:
        # créer un curseur
        cur = conn.cursor()
        
        # Execute a SQL INSERT command
        cur.execute(sql)

        # Commit (transactionnal mode is by default)
        conn.commit()
        
        count = cur.rowcount    
        # print(count, "successfully ")
        if count == 1:
            return True
        else:
            return False
    except (Exception, psycopg2.Error) as error:
        print(error)
        return False
    finally:
        if conn:
            # fermer la communication avec PostgreSQL
            cur.close()


def inserer(conn, sql):
    try:
        # créer un curseur
        cur = conn.cursor()
        
        # Execute a SQL INSERT command
        cur.execute(sql)
        
        # Commit (transactionnal mode is by default)
        conn.commit()
        count = cur.rowcount    
        # print(count, "successfully ")
        if count == 1:
            return True
        else:
            return False

    except (Exception, psycopg2.Error) as error:
        print(error)
        return False
    finally:
        if conn:
            # fermer la communication avec PostgreSQL
            cur.close()

def verifier_existant(conn, attributes, conds):
    sql = "SELECT exists(SELECT 1 FROM Utilisateur WHERE "
    len_attr = len(attributes)
    for i in range(len_attr):
        sql += str(attributes[i]) + " = '" + str(conds[i]) + "'"
        if i < len_attr - 1:
            sql += " AND "
    sql += " );"
    
    data = selectionner(conn, sql)
    return data[0][0]


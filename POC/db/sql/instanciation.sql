INSERT INTO Utilisateur (pseudo, mdp, date_naissance, solde, mail)
VALUES ('newton', 'lapomme45', '1985-02-02', 100, 'newton@etu.utc.fr');
INSERT INTO Utilisateur (pseudo, mdp, date_naissance, solde, mail)
VALUES ('lavoisier', 'RseperdTsetranforme', '1996-02-02', 0, 'lavoisier@etu.utc.fr');
INSERT INTO Utilisateur (pseudo, mdp, date_naissance, solde, mail)
VALUES ('hector22', 'hectorlecastor', '2000-07-02', 4, 'hector.malin@etu.utc.fr');
INSERT INTO Utilisateur (pseudo, mdp, date_naissance, solde, mail)
VALUES ('michelle', 'ILOVEBARACK', '1964-12-17', 10000, 'michelle.obama@etu.utc.fr');

INSERT INTO Annonce(id,utilisateur,titre,description,validation) VALUES(1,'newton','Pomme','Je cherche la pomme de la gravité.',FALSE);
INSERT INTO Demande(annonce) VALUES(1);
INSERT INTO Bien(annonce,nom,etat,quantite) VALUES(1,'Pomme','neuf',1);

INSERT INTO Annonce(id,utilisateur,titre,description,validation) VALUES(2,'lavoisier','Atome Helium','Deux pour le prix dun.',FALSE);
INSERT INTO Offre(id,prix,negociable) VALUES(2,20,FALSE);
INSERT INTO Bien(annonce,nom,etat,quantite) VALUES (2,'Atome Helium','usage',2);

INSERT INTO Annonce(id,utilisateur,titre,description,validation) VALUES(3,'newton','Savoir','Je suis à la recherche d une meilleure théorie.',FALSE);
INSERT INTO Demande(annonce) VALUES(3);
INSERT INTO Service(annonce,nom,date,lieu) VALUES(3,'seance de brainstorming',NULL,NULL);

INSERT INTO Annonce (id,utilisateur,titre,description,validation)  VALUES(4,'michelle','Massage','Je fais des massages à la française. Prix indiqué à l heure.',FALSE);
INSERT INTO Offre(id,prix,negociable) VALUES(4,50,FALSE);
INSERT INTO Service(annonce,nom,date,lieu) VALUES(4,'massage',NULL,'Benjamin Franklin');

INSERT INTO annonce(utilisateur, titre, description, validation) VALUES ('cat','hello', 'helo', FALSE);
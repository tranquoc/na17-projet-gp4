CREATE TABLE IF NOT EXISTS  Utilisateur
(
    pseudo VARCHAR(50) PRIMARY KEY,
    mdp VARCHAR(50) NOT NULL CHECK (LENGTH(mdp) >= 8),
    date_naissance DATE NOT NULL,
    solde FLOAT NOT NULL CHECK ( solde >= 0 ),
    mail VARCHAR(50) CONSTRAINT chk_mail CHECK ( mail ~* '^[A-Za-z0-9._%-]+@etu.utc.fr$' ) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS Annonce
(
    id SERIAL PRIMARY KEY,
    utilisateur VARCHAR(50) NOT NULL REFERENCES Utilisateur(pseudo) , 
    titre VARCHAR(50) NOT NULL,
    description VARCHAR(100) NOT NULL,
    validation BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS Offre
(
id integer NOT NULL REFERENCES Annonce(id),
prix FLOAT NOT NULL CHECK (prix BETWEEN 0.01 AND 2000),
negociable BOOLEAN NOT NULL,
PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS Demande
(
    annonce INTEGER NOT NULL REFERENCES Annonce(id)
);

CREATE TABLE IF NOT EXISTS Bien
(
    annonce INTEGER NOT NULL REFERENCES Annonce(id),
    nom VARCHAR(50),
    etat VARCHAR(10) NOT NULL CHECK (etat IN ('neuf', 'tres bon', 'bon', 'usage')),
    quantite INTEGER NOT NULL CHECK (quantite >= 1)
);

CREATE TABLE IF NOT EXISTS Service
(
    annonce INTEGER NOT NULL REFERENCES Annonce(id),
    nom VARCHAR(50),
    date DATE,
    lieu VARCHAR(50)
);

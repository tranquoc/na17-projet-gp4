import getpass
import datetime
from db.requetes.commande import verifier_existant

def entrez_text(cond, length = 50):
    entrez = input(cond)
    if len(entrez) > length:
        return entrez_text(cond, length)
    return entrez

def entrez_boolen(cond):
    while True:
        try:
           return {"true":True,"false":False}[input(cond).lower()]
        except KeyError:
           print ("Rentrez True or False!")

def entrez_int(cond="Entrez: "):
    entrez = input(cond)
    try:
        entrez = int(entrez)
        return entrez
    except ValueError:
        print("C'est pas nombre interger")
        return False

def entrez_float(cond):
    entrez = input(cond)
    try:
        entrez = float(entrez)
        return entrez
    except ValueError:
        print("C'est pas nombre float")
        return False

def entrez_nouvelle_pseudo(conn):
    pseudo = input("Pseudo: ")
    attributes = ["pseudo"]
    conds = [pseudo]
    
    if verifier_existant(conn, attributes, conds):
        return entrez_pseudo(conn)
    
    return pseudo

def entrez_mdp(flag = True):
    tmp = "Entrez mot de passe (> 8 char): "
    if not flag:
        tmp = "R" + tmp
    mdp = getpass.getpass(tmp)
    if len(mdp) > 7:
        return mdp
    else:
        return entrez_mdp()

def entrez_verifier_mdp():
    mdp = entrez_mdp()
    
    tmp = entrez_mdp(False)
    
    if tmp == mdp:
        return mdp
    else:
        return entrez_verifier_mdp()
        
    

def entrez_date(cond=""):
    date = input('Date {} (AAAA-MM-JJ): '.format(cond))
    try:
        datetime.datetime.strptime(date, '%Y-%m-%d')
        return date
    except ValueError:
        print ("Format de données incorrect, doit être YYYY-MM-DD")
        return False

def date_now():
    return datetime.date.today()

def entrez_date_naissance():
    date_naissance = entrez_date("naissance")

    if date_naissance:
        an, mois, jour = map(int, date_naissance.split('-'))
        date = datetime.date(an, mois, jour)
        
        now = date_now()
        
        if date > now:
            return entrez_date_naissance()
        else:
            return date_naissance
    else:
        return entrez_date_naissance()
def entrez_solde():
    solde = entrez_float("Solde(>=0): ")
    if solde is False or solde < 0:
        return entrez_solde()
    return solde

def entrez_mail(conn):
    mail = input("Mail(xxx@etu.utc.fr): ")   
    attributes = ["mail"]
    conds = [mail]

    if mail[-11::] != "@etu.utc.fr" or verifier_existant(conn,attributes,conds):
        return entrez_mail(conn)
    
    return mail
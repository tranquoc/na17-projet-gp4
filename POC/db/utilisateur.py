from db.requetes.commande import verifier_existant
from db.requetes.utilisateur import *
from db.entrez import *

def inserer_utilisateur(conn):
    print("\n")
    print("===== S'inscrire ======")
    

    pseudo = entrez_nouvelle_pseudo(conn)
    mdp = entrez_verifier_mdp()
    date_naissance = entrez_date_naissance()
    solde = entrez_solde()
    mail = entrez_mail(conn)
    
    values = (pseudo, mdp, date_naissance, solde, mail)   

    requete_inserer_utilisateur(conn,values)

def connecte_utilisateur(conn):
    print("\n")
    print("===== Se connecte ======")
    pseudo = input("Pseudo: ")
    mdp = entrez_mdp()
    
    attributes = ["pseudo", "mdp"]
    conds = [pseudo,mdp]

    if not verifier_existant(conn,attributes,conds):
        print("Entrez incorrect mot de pass ou pseudo")
        return None
    
    return pseudo
        
def afficher_info(infos):
    print("Pseudo: ",infos[0])
    print("Date naissance: ",infos[1])
    print("Solde: ",infos[2])
    print("Mail: ",infos[3])


def info_utilisateur(conn, pseudo):
    infos = requete_select_utilisatuer(conn,pseudo)
    
    print("\n")
    print("===== INFORMATION ======")
    afficher_info(infos)
    
def info_tout_utilisateur(conn):
    infos = requete_select_tout_utilisatuer(conn)
    print(infos)
    print("\n")
    print("===== INFORMATION ======")
    i = 0
    for info in infos:
        i += 1
        print(i)
        afficher_info(info)
        print("\n")


def maj_utilisateur(conn, pseudo):

    print("\n")
    print("===== MISE A JOUR ======")

    tmp = {1: "pseudo", 2: "mdp", 3: "date_naissance", 4: "solde", 5: "mail"}
    for key, value in tmp.items():
        print(key, value)
    
    choix = entrez_int()
    while choix is False or choix < 1 or choix > 5:
        choix = entrez_int()
    
    print("\n")
    
    value = None

    if choix == 1:
        value = entrez_nouvelle_pseudo(conn)
    elif choix == 2:
        value = entrez_verifier_mdp()
    elif choix == 3:
        value = entrez_date_naissance()
    elif choix == 4:
        value = entrez_solde()
    elif choix == 5:
        value = entrez_mail(conn)
    
    
    requete_mise_a_jour_table(conn, "Utilisateur", tmp[choix], value, "pseudo", pseudo)

    if choix == 1:
        return value
    return None
from db.requetes.annonce import *
from db.entrez import entrez_text, entrez_boolen

def inserer_annonce(conn, pseudo):
    print("\n")
    print("===== S'inscrire l'annonce ======")
    
    titre = entrez_text("Entrez titre: ")
    description = entrez_text("Entrez description: ", 100)
    validation = entrez_boolen("Entrez validation: ")
    
    values = (pseudo, titre, description, validation)   

    requete_inserer_annonce(conn,values)
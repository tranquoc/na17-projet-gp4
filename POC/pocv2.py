#!/usr/bin/python3

# trop simple
# S'il est écrit dans un fichier, il sera très long et entraînera un programme lentement

import psycopg2

HOST = "localhost"
USER = "me"
PASSWORD = "secret"
DATABASE = "mydb"

# Open connection
conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))

# Open a cursor to send SQL commands
cur = conn.cursor()

# Choose action
option = input("Enter the number of your chosen option ; \n1 - SELECT\n2 - INSERT\n")
option = int(option)

# Doit se souvenir des regles SQL
sql = input("Entrez votre requête SQL ")

# Execute the SQL command
cur.execute(sql)
if (option == 1) :
    cur.execute(sql)    
    print(cur.fetchall())
else :
    if (option == 2) :
        # Commit (transactionnal mode is by default)
        conn.commit()
        print("Done")
        
# pas fermez cur

# Close connection
conn.close()
CREATE TABLE Annonce
(
    id SERIAL PRIMARY KEY,
    utilisateur VARCHAR(50) NOT NULL REFERENCES Utilisateur(pseudo) , 
    titre VARCHAR(50) NOT NULL,
    description VARCHAR(100) NOT NULL,
    validation BOOLEAN NOT NULL,
    service JSON,
    bien JSON 
);

--Nous gerons avec JSON les compositions (Bien et Service), il faudra vérifier avec une contrainte complexe qu'il ne peut exister un bien ET un service en même temps

--INSERT INTO Annonce VALUES(4,'michelle','Massage','Je fais des massages à la française. Prix indiqué à l heure.',FALSE);
--INSERT INTO Service VALUES(4,'massage',NULL,'Benjamin Franklin');

--Avant nous devions faire deux insertions puisqu'il y avait deux tables

INSERT INTO Annonce 
VALUES(
4,
'michelle',
'Massage',
'Je fais des massages à la française. Prix indiqué à l heure.',
FALSE,
'{"nom":"massage", "date":NULL, "lieu":"Benjamin Franklin"}'
 );

--Il y a maintenant qu'une seule requête, l'optinnalité de bien et service est exprimée lors de l'insertion (ici bien est NULL, nous n'insérons qu'un service)
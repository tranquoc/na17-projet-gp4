# Modèle logique de donnée

## Utilisateur (#pseudo:string, mdp: string, date_naissance : date, solde : float, mail : string )
### Contraintes :
* clé candidate : mail
* pseudo, mdp, mail, date_de_naissance, solde, type NOT NULL
* la chaîne mdp doit faire au moins 8 caractères
* la chaîne mail doit être au format "@etu.utc.fr",  "@utc.fr" ou "@hds.utc.fr"
* solde >= 0

Ensemble des DFE : 
* pseudo -> mdp
* pseudo -> date_naissance
* pseudo ->mail
* pseudo ->solde
* mail -> mdp
* mail -> pseudo
* mail -> date_naissance
* mail ->solde

clés: pseudo, mail

Donc BCNF

BCNF

## Administrateur (#pseudo => Utilisateur)

BCNF

## Annonce (#id: int, validation:bool, utilisateur => Utilisateur, titre: string, description:string, negociable: boolean ) 
### Contraintes : 
* validation, utilisateur, titre, description, type_offre, NOT NULL
* Validation ne peut être changé que par des administrateurs. La relation originellement 1:N sur l'UML n'aboutit pas sur une clé étrangère car la validation se gère au niveau de la gestion des droits (GRANT) de la base de donnée. Idem pour commentaire.

BCNF


## Offre (#annonce => Annonce, prix: float)
### Contraintes:
    * Le prix est compris entre 0.01 et 10000 (exprimé en euros)
Union(Projection(Offre,annonce), Projection(Bien,annonce)) = (Projection(Annonce,id) <- annonce classe abstraite non instanciable
Jointure(Offre,demande, Offre.annonce=Bien.annonce)={} <- héritage exclusif


BCNF
    
## Demande(#annonce => Annonce)

BCNF

## Enchère (#numero: int, #offre=>Offre, #utilisateur => Utilisateur, montant: float, date: datetime)
### Contraintes :
* date NOT NULL
* montant > 0
* Lors de la mise à jour du montant par un utilisateur, la nouvelle valeur est supérieure à l'ancienne
* montant, date,NOT NULL
* numéro est clé locale : il peut y avoir plusieurs enchères sur une même offre par un utilisateur

La clé est composée des trois attributs et aucun sous-ensemble de la clé ne détermine un autre attribut. Montant nedétermine pas date et inversement. Enfin, ni montant ni date ne détermine un attribut de la seule clé candidate.
BCNF

## Bien (#annonce => Annonce, nom:string, etat: {neuf, tres bon, bon, usage}, quantite: int)
### Contraintes:
    * etat, quantite NOT NULL
    * quantité >=1

BCNF 

## Service (#annonce => Annonce, nom:string, date: date, lieu: string) 
### Contraintes : 
Union(Projection(Service,annonce), Projection(Bien,annonce)) = (Projection(Annonce,id) <- annonce classe abstraite non instanciable
Jointure(Service,Bien, Service.annonce=Bien.annonce)={} <- équivalent de l'"exclusivité de la composition"
Possibilité d'avoir deux annonce avec le meme titre, meme date, meme lieu

BCNF

## Categorie (#nom: string)

BCNF

## Tag_Annonce (#cat => Categorie, #annonce=>Annonce)

BCNF

## Transaction( #id_annonce => Annonce, emetteur => Utilisateur, receveur => Utilisateur, date_paiement:datetime, statut : {en cours, terminee}, prix: float)
### Contraintes : 

* RESTRICTION(Transaction, emetteur = receveur) = {}
* statut, emetteur, receveur NOT NULL
* Le posteur de l'annonce à laquelle est rattachée la transaction est l'emetteur ou le receveur (selon qu'il s'agisse d'une offre ou d'une demande)

BCNF


## Action (#id: int, utilisateur=>Utilisateur, date: datetime, montant:float, type:{rechargement, retrait}) 
### Contraintes:
* date, montant, type, utilisateur NOT NULL
* montant > 0
* On a choisi une clé artificielle car on a estimé que (Utilisateur, date) n'était pas unique (soucis d'implémentation ?)

BCNF

## Message (#id:int, emetteur => Utilisateur, receveur => Utilisateur, date : datetime, contenu : string)
### Contraintes :
* contenu NOT NULL,
* On ne peut pas s'envoyer un message à soi même : RESTRICTION(Message, receveur=acteur)={}

BCNF

## Commentaire (#transaction=>Transaction, #id_posteur => Administrateur, titre : string, contenu : string, date: datetime, note: integer, valide=bool)
### Contraintes : 
* Valide ne peut être modifié que par un adminstrateur. Voir contraintes annonce.

BCNF


R2 = JOINTURE(Transaction, Annonce, Transaction.id_annonce=Annonce.id)



 (commentaires et transactions associée où l'annonce associée est une offre)
R2 = PROJECTION(R1, id_emetteur)
R3 = PROJECTION(R1, id_posteur)
* titre, contenu, note NOT NULL
* Les commentaires ne portent que sur des transactions effectuées: R1= Jointure(Commentaire, Transaction,Commentaire.id_transac=Transaction.id)  et R2 = Projection(R1,Transaction.statut='en cours') = {}
* Note comprise entre 0 et 5


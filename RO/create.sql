CREATE OR REPLACE TYPE TCategorie AS OBJECT (
	nom VARCHAR2(50)
);
/

CREATE TABLE Categorie OF TCategorie (
	PRIMARY KEY	(nom)
);

CREATE OR REPLACE TYPE refCategorie AS OBJECT (
	refCategorie REF TCategorie
);
/

CREATE OR REPLACE TYPE refCategorieListe AS TABLE OF refCategorie;
/

CREATE OR REPLACE TYPE TBien AS OBJECT (
    nom VARCHAR2(50),
    etat VARCHAR2(10),
    quantite NUMBER(5)
);
/ 


CREATE OR REPLACE TYPE TService AS OBJECT (
    nom VARCHAR2(50),
    dservice DATE,
    lieu VARCHAR2(50)
);
/


CREATE OR REPLACE TYPE TAnnonce AS OBJECT
(
    id NUMBER(1),
    utilisateur VARCHAR2(50), 
    titre VARCHAR2(50),
    description VARCHAR2(100),
    validation CHAR(1),
    bien TBien,
    service TService,
    categorie refCategorieListe,
    type VARCHAR2(10),
    prix NUMBER(5),
    negociable VARCHAR2(4)
)
;
/

CREATE TABLE Utilisateur
(
    pseudo VARCHAR2(50) PRIMARY KEY,
    mdp VARCHAR2(50) NOT NULL CHECK (LENGTH(mdp) >= 8),
    date_naissance DATE NOT NULL,
    solde FLOAT NOT NULL CHECK ( solde >= 0 ),
    mail VARCHAR2(50) UNIQUE NOT NULL
);


CREATE TABLE Annonce OF TAnnonce
(
    id PRIMARY KEY,
    utilisateur NOT NULL REFERENCES Utilisateur(pseudo), 
    titre NOT NULL,
    description NOT NULL,
    validation NOT NULL CHECK (validation IN ('T', 'F')),
    CONSTRAINT bOUs CHECK(bien IS NOT NULL OR service IS NOT NULL),
    CONSTRAINT pasbETs CHECK(bien IS NULL OR service IS NULL),
    CONSTRAINT offreoupas CHECK(type in ('offre','demande')),
    CONSTRAINT prixsuroffre CHECK((prix IS NULL AND type = 'demande') OR (prix IS NOT NULL AND type='offre')),
    CONSTRAINT negooupas CHECK((negociable IS NULL AND type='demande') OR ((negociable='OUI' OR negociable='NON') AND type='offre')),
    CONSTRAINT prixlimite CHECK(prix <= 10000 AND prix >=0)
)
NESTED TABLE categorie STORE AS ntcategorie;


INSERT INTO CATEGORIE(nom) values ('Velo');
INSERT INTO CATEGORIE(nom) values ('Jeux');
INSERT INTO CATEGORIE(nom) values ('Musique');
INSERT INTO CATEGORIE(nom) values('Retro');

INSERT INTO Utilisateur (pseudo, mdp, date_naissance, solde, mail)
VALUES ('newton', 'lapomme45', TO_DATE('1985-02-02', 'YYYY-MM-DD'), 100, 'newton@etu.utc.fr');
INSERT INTO Utilisateur (pseudo, mdp, date_naissance, solde, mail)
VALUES ('lavoisier', 'RseperdTsetranforme', TO_DATE('1992-03-16', 'YYYY-MM-DD'), 0, 'lavoisier@etu.utc.fr');
INSERT INTO Utilisateur (pseudo, mdp, date_naissance, solde, mail)
VALUES ('hector22', 'hectorlecastor', TO_DATE('2000-07-02', 'YYYY-MM-DD'), 4, 'hector.malin@etu.utc.fr');

    
    DECLARE
    x1 REF TCategorie;
    x2 REF TCategorie;
    
    BEGIN
    
    SELECT REF(r) INTO x1
    FROM Categorie r
    WHERE r.nom='Jeux'
    ;
    
    SELECT REF(r) INTO x2
    FROM Categorie r
    WHERE r.nom='Retro'
    ;
    
    INSERT INTO Annonce(id,utilisateur,titre,description,validation,bien,service,categorie,type,prix,negociable) VALUES(
    1,
    'newton',
    'Vend une gameboy',
    'Je vends ma magnifique GBA édition 1987 pour cause de déménagement',
    'T',
    TBien('GBA AS 101','tres bon',1),
    NULL,
    refCategorieListe(refCategorie(x1),refCategorie(x2)),
    'offre',
    150,
    'OUI'
    );
    
    END;

-- 

    DECLARE
    x1 REF TCategorie;
    
    BEGIN
    
    SELECT REF(r) INTO x1
    FROM Categorie r
    WHERE r.nom='Velo'
    ;
    
    INSERT INTO Annonce(id,utilisateur,titre,description,validation,bien,service,categorie,type,prix,negociable) VALUES(
    2,
    'hector22',
    'Vend un Velo de course',
    'Je me débarasse de mon btween edition 2019 X200. Jamais servi. Cause double usage(cadeau).',
    'T',
    TBien('BTWEEN X200','neuf',1),
    NULL,
    refCategorieListe(refCategorie(x1)),
    'offre',
    '250',
    'NON'
    );
    
    END;


--Etant donné que les requêtes seront centrées sur la table Annonce, nous utilisons une collection imbriquée de référence à des OID de la table Categorie. Nous évitons ainsi une jointure.
--On ne pourra pas gérer les doublons avec des références à des OID, il faudra gérer cela au niveau applicatif.


--Exemple de requête:

SELECT A.id, A.utilisateur, A.titre, A.description, A.validation, A.bien.nom, A.bien.etat, A.bien.quantite, A.service.nom, c.refCategorie.nom
FROM Annonce A, TABLE(A.categorie) c;

--Ici, plus besoin d''effectuer de jointures pour obtenir la catégorie et les détails du service/bien de l''annonce.




BEGIN TRANSACTION;

DROP TABLE IF EXISTS Utilisateur, Administrateur, Annonce, Categorie, TagAnnonce, Offre, Demande, Enchere, Bien, Service, Transaction, Commentaire, Message, Action, vAnnonce;

CREATE TABLE Utilisateur
(
    pseudo VARCHAR(50) PRIMARY KEY,
    mdp VARCHAR(50) NOT NULL CHECK (LENGTH(mdp) >= 8),
    date_naissance DATE NOT NULL,
    solde FLOAT NOT NULL CHECK ( solde >= 0 ),
    mail VARCHAR(50) CONSTRAINT chk_mail CHECK ( mail ~* '^[A-Za-z0-9._%-]+@etu.utc.fr$' ) UNIQUE NOT NULL
);

CREATE TABLE Administrateur 
(
pseudo VARCHAR(50)  PRIMARY KEY REFERENCES Utilisateur(pseudo)
);

CREATE TABLE Annonce
(
    id SERIAL PRIMARY KEY,
    utilisateur VARCHAR(50) NOT NULL REFERENCES Utilisateur(pseudo) , 
    titre VARCHAR(50) NOT NULL,
    description VARCHAR(100) NOT NULL,
    validation BOOLEAN NOT NULL
);


CREATE TABLE Categorie
(
    nom VARCHAR(50) PRIMARY KEY
);

CREATE TABLE TagAnnonce
(
    categorie VARCHAR(50) REFERENCES Categorie(nom),
    annonce INTEGER REFERENCES Annonce(id),
    PRIMARY KEY(categorie, annonce)
);

CREATE TABLE Offre
(
id integer NOT NULL REFERENCES Annonce(id),
prix FLOAT NOT NULL CHECK (prix BETWEEN 0.01 AND 2000),
negociable BOOLEAN NOT NULL,
PRIMARY KEY(id)
);

CREATE TABLE Demande
(
    annonce INTEGER NOT NULL REFERENCES Annonce(id)
);

CREATE TABLE Enchere
(
    numero SERIAL PRIMARY KEY,
    offre INTEGER NOT NULL REFERENCES Offre(id),
    utilisateur VARCHAR(50) NOT NULL REFERENCES Utilisateur(pseudo),
    montant FLOAT CHECK (montant > 0),
    date DATE NOT NULL
);

CREATE TABLE Bien
(
    annonce INTEGER NOT NULL REFERENCES Annonce(id),
    nom VARCHAR(50),
    etat VARCHAR(10) NOT NULL CHECK (etat IN ('neuf', 'tres bon', 'bon', 'usage')),
    quantite INTEGER NOT NULL CHECK (quantite >= 1)
);

CREATE TABLE Service
(
    annonce INTEGER NOT NULL REFERENCES Annonce(id),
    nom VARCHAR(50),
    date DATE,
    lieu VARCHAR(50)
);

CREATE TABLE Transaction
(
    id_transaction SERIAL PRIMARY KEY,
    emetteur VARCHAR(50) NOT NULL REFERENCES Utilisateur(pseudo),
    receveur VARCHAR(50) NOT NULL REFERENCES Utilisateur(pseudo), 
    date_paiement DATE,
    statut VARCHAR(8) NOT NULL CHECK (statut IN ('en cours','terminee')),
    prix FLOAT,
    annonce INTEGER NOT NULL REFERENCES Annonce(id),
    CONSTRAINT chkEmetteurReceveur CHECK(emetteur!=receveur)
);

CREATE TABLE Commentaire (
    id_transac INTEGER,
    FOREIGN KEY (id_transac) REFERENCES Transaction(id_transaction),
    posteur VARCHAR(50), 
    FOREIGN KEY (posteur) REFERENCES Utilisateur(pseudo),
    titre VARCHAR(50) NOT NULL,
    contenu VARCHAR(200) NOT NULL,
    c_date DATE NOT NULL,
    note INTEGER CHECK(1 <= note AND note <= 5) NOT NULL,
    PRIMARY KEY(id_transac,posteur)
);

CREATE TABLE Message
(
    id INTEGER PRIMARY KEY,
    c_date DATE,
    contenu VARCHAR NOT NULL,
    emetteur VARCHAR,
    receveur VARCHAR,
    FOREIGN KEY (emetteur) REFERENCES Utilisateur(pseudo),
    FOREIGN KEY (receveur) REFERENCES Utilisateur(pseudo),
    CHECK(emetteur != receveur)
);

CREATE TABLE Action
(
    id SERIAL PRIMARY KEY,
    a_date DATE NOT NULL,
    montant FLOAT NOT NULL CHECK(montant > 0),
    a_type VARCHAR CHECK(a_type IN ('rechargement', 'retrait')) NOT NULL,
    utilisateur VARCHAR NOT NULL,
    FOREIGN KEY(utilisateur) REFERENCES Utilisateur(pseudo)
);

COMMIT;


--Vues concrètes

BEGIN TRANSACTION;

DROP TABLE IF EXISTS vAnnonce;

CREATE TABLE vAnnonce AS  
SELECT *
FROM Annonce A JOIN TagAnnonce T ON A.id = T.annonce
WHERE A.validation = TRUE;

COMMIT;

BEGIN TRANSACTION;

DROP TABLE IF EXISTS vEnchereMax, vInfoEnchereMax;

CREATE TABLE vEnchereMax AS
SELECT offre, MAX(montant) AS prixMax
FROM Enchere 
GROUP BY offre;

CREATE TABLE vInfoEnchereMax AS
SELECT V.offre, V.prixMax, E.utilisateur
FROM vEnchereMax V INNER JOIN Enchere E ON (V.offre = E.offre AND V.prixMax = E.montant);

COMMIT;